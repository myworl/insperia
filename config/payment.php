<?php 

return [
    'default_payment' => 'tinkoff',
    'options' => [
        'tinkoff' => [
            'tinkoff_url' => env('TINKOFF_URL', false),
            'tinkoff_terminal' => env('TINKOFF_TERMINAL', false),
            'tinkoff_secret_key' => env('TINKOFF_SECRET_KEY', false),
        ]
    ]
];