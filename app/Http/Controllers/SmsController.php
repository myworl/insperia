<?php

namespace App\Http\Controllers;

use App\Models\PhoneConfirm;
use App\Models\User;
use App\Utils\OutputService;
use Illuminate\Http\Request;
use App\Utils\SmsaeroApiV;
use Illuminate\Support\Facades\Validator;

class SmsController extends Controller
{

    public function smsSend(Request $request)
    {
        $code_random = rand(0,9).rand(0,9).rand(0,9).rand(0,9);
        $api_key = config('app.sms_api_key');
        $email = config('app.sms_email');
        $sign = 'SMS Aero';
        $code_text = 'Ваш код подтверждения '.$code_random;

        $this->validate($request, [
            'phone' => 'required|integer|min:10',
        ]);
        $phone_val = $request->get('phone');
        $id = auth()->user()->id;

        if (PhoneConfirm::where("users_id", $id)->exists())
        {
            return OutputService::sendJson([
                "error" =>   'Код уже был отправлен на номер'.$phone_val
            ], 400);
        }

        $phone_confirm = new PhoneConfirm();
        $phone_confirm->users_id = $id;
        $phone_confirm->phone = $phone_val;
        $phone_confirm->code = $code_random;
        $phone_confirm->save();

        $smsaero_api = new SmsaeroApiV($email, $api_key, $sign);
        $smsaero_api->send($phone_val,$code_text);
        return OutputService::sendJson([
            "message" =>  'Код отправлен на номер '.$phone_val
        ]);

    }

    public function smsVerification(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|integer|digits:4',
        ]);
        $errors = $validator->errors();
        if($errors->all()){
            return OutputService::sendJson([
                "error" =>  $errors->first('code'),
            ],400);
        }
        $code = $request->get('code');
        $user_id = auth()->user()->id;
        $phone_confirm = PhoneConfirm::latest()->where('users_id',$user_id)->first();
        if(empty($phone_confirm->code)){
            return OutputService::sendJson([
                "error" =>   'Пользователь не нажимал на кнопку подтвердить телефон'
            ], 404);
        }
        if($phone_confirm->code == $code){
            $user = User::find($user_id);
            $user->phone_active = 1;
            $user->save();
            PhoneConfirm::where('users_id',$user_id)->delete();
            return OutputService::sendJson([
                "message" =>   'Номер подтвержден!'
            ]);
        }
        return OutputService::sendJson([
            "error" =>   'Не верный код'
        ], 400);

    }

}
