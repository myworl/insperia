<?php

namespace App\Http\Controllers;

use App\Http\Requests\Module\ModuleRequest;
use App\Http\Requests\ModuleLessonRequest;
use App\Http\Resources\Module\ModuleLessonResource;
use App\Models\Module;
use App\Models\ModuleLesson;
use App\Utils\OutputService;
use App\Utils\ProgressService;

class ModuleLessonController extends Controller
{
    public function moduleLessonGet(ModuleRequest $request){
        $module_id = $request->get('module_id');
        $module = Module::where('id', $module_id)->with('moduleLesson')->first();

        ProgressService::setProgressLimits(auth()->id(), module_id: $module_id);

        return OutputService::sendJson( new ModuleLessonResource($module));
    }

    public function moduleLessonCreate(ModuleLessonRequest $request)
    {
        $module_id = $request->get('module_id');
        $lesson_id = $request->get('lesson_id');

        $module_lesson_id = ModuleLesson::where('modules_id', $module_id)->where('lessons_id', $lesson_id)->first();
        if ($module_lesson_id != null) {
            return OutputService::sendJson([
                'message' => 'Такая запись уже существует',
            ]);
        }

        $module_lesson = new ModuleLesson();
        $module_lesson->modules_id = $module_id;
        $module_lesson->lessons_id = $lesson_id;
        $module_lesson->save();

        $module_lesson_get = ModuleLesson::latest()
            ->with(
                'lesson',
                'lesson.lessonType',
                'lesson.lessonWebinar',
                'lesson.lessonVideo'
            )
            ->first();

        return OutputService::sendJson(
            $module_lesson_get->lesson
        );
    }

    public function moduleLessonDelete(ModuleLessonRequest $request)
    {
        $module_id = $request->get('module_id');
        $lesson_id = $request->get('lesson_id');

        ModuleLesson::where('modules_id', $module_id)->where('lessons_id', $lesson_id)->first()?->delete();
        
        return OutputService::sendJson([
            'message' => 'Запись удалена',
        ]);
    }
}
