<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Basket;
use App\Models\OrderList;
use App\Models\OrdersPosition;
use App\Utils\OutputService;
use App\Utils\Payment;
use Exception;
use App\Http\Requests\PaymentCheckRequest;
use App\Models\CourseUser;
use App\Models\Transaction;
use Carbon\Carbon;

class PaymentController extends Controller
{
    public function create(Request $request)
    {
        $user_id  = auth()->user()->id;
        $order_id = time().'-'.$user_id;

        $basket = Basket::where('user_id', $user_id)
                            ->with('baskToOption')
                            ->get();

        if($basket->count() == 0)
        {
            return OutputService::sendJson(['Ваша корзина пуста'], OutputService::CODE_UNPROCESSABLE_ENTITY);
        }

        $order_total_price = $basket->sum('baskToOption.price');
   
        $order_total_price = $order_total_price * 100;

        $order_list = new OrderList();
        $order_list->user_id = $user_id;
        $order_list->order_id = $order_id;
        $order_list->cost = $order_total_price;
        $order_list->status = OrderList::STATUS_NEW;
        $order_list->save();

        $order_items = [];
        foreach ($basket as $basket_item)
        {
            $order_items[] = [
                'orders_lists_id' => $order_list->id,
                'course_tarif_options_id' => $basket_item->course_tarif_option_id,
                'price' => $basket_item->baskToOption->price,
                'old_price' => $basket_item->baskToOption->old_price
            ];
        }

        OrdersPosition::insert($order_items);

        $payment = new Payment($request->get('payment'));

        try {
            $payment->create($order_list);
            Basket::where('user_id',$user_id)->delete();
            return OutputService::sendJson(['payment_url' => $payment->getRedirectUrl()]);
        }catch(Exception $e){
            return OutputService::sendJson(['error' => $e->getMessage()], OutputService::CODE_NOT_FOUND);
        }
    }

    public function check(PaymentCheckRequest $request)
    {
        $order_list = OrderList::where('order_id', $request->get('order_id'))->first();
        if (null === $order_list)
        {
            return OutputService::sendJson(['Заказ не найден'], OutputService::CODE_NOT_FOUND);
        }

        if (null === $order_list->transaction_id)
        {
            return OutputService::sendJson(['Оплата не найден'], OutputService::CODE_NOT_FOUND);
        }

        $transaction = Transaction::where('orders_list_id', $order_list->id)->where('id', $order_list->transaction_id)->first();
        if (null === $transaction)
        {
            return OutputService::sendJson(['Оплата не найден'], OutputService::CODE_NOT_FOUND);
        }

        $payment = new Payment($request->input('type'));
        $payment_status = $payment->check($transaction);
        $old_status = $transaction->payment_status;
        $transaction->payment_status = $payment_status;
        $transaction->save();

        if ($transaction->payment_status == Transaction::STATUS_APPROVED && $old_status == Transaction::STATUS_WAITING)
        {
            $order_position = OrdersPosition::where('orders_lists_id', $order_list->id)->get();
            $new_user_courses = [];
            foreach ($order_position as $position)
            {
                $new_user_courses[] = [
                    'users_id' => $order_list->user_id,
                    'options_id' => $position->course_tarif_options_id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
            }

            if (count($new_user_courses) > 0)
            {
                CourseUser::insert($new_user_courses);
            }

            return redirect()->away(config('app.front_url').'/payment/success');
        }else{
            return redirect()->away(config('app.front_url').'/payment/fail');
        }
    }
}
