<?php

namespace App\Http\Controllers;

use App\Http\Requests\AvatarRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Utils\OutputService;
use Exception;
use Illuminate\Support\Facades\Storage;

class AvatarController extends Controller
{

    const USER_AVATAR_DIR = 'public/users';

    public function avatarAdd(AvatarRequest $request)
    {
        $user = User::find(auth()->id());

        if (null !== $user->avatar) {
            try {
                Storage::delete(self::USER_AVATAR_DIR . '/' . $user->avatar);
            } catch (Exception $e) {}
        }

        $extention = $request->file('file')->getClientOriginalExtension();
        $fileNameToStore = "id_" . $user->id . "_" . time() . "." . $extention;
        $request->file('file')->storeAs(self::USER_AVATAR_DIR, $fileNameToStore);

        $user->avatar = $fileNameToStore;
        $user->save();

        return OutputService::sendJson(
            new UserResource($user),
        );
    }

    public function avatarDelete()
    {
        $user = User::find(auth()->id());

        try {
            Storage::delete(self::USER_AVATAR_DIR . '/' . $user->avatar);
        } catch (Exception $e) {}

        $user->avatar = null;
        $user->save();

        return OutputService::sendJson();
    }
}
