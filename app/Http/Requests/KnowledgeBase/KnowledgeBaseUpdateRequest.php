<?php

namespace App\Http\Requests\KnowledgeBase;

use App\Utils\RequestPrepare;

class KnowledgeBaseUpdateRequest extends RequestPrepare
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "section" => "required|string",
            "question" => "required|string",
            "answer" => "required|string",
            "order" => "required|integer|min:1",
        ];
    }
}
