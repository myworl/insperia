<?php

namespace App\Http\Requests\Subject;

use App\Utils\RequestPrepare;

class SubjectStoreRequest extends RequestPrepare
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string",
            "description" => "required|string",
            "order" => "required|integer|min:1",
        ];
    }
}
