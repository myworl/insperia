<?php

namespace App\Http\Requests;

use App\Utils\RequestPrepare;

class AuthUpdateRequest extends RequestPrepare
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:12',
            'lastName' => 'nullable|string|max:12',
            'email' => 'nullable|email|max:40',
            'gender' => 'nullable|string|max:8',
            'instagram' => 'nullable|string|max:40',
            'telegram' => 'nullable|string|max:40',
            'phone' => 'nullable|phone|max:89999999999',
            'birthday' => 'nullable|string|max:20',
            'city' => 'nullable|string|max:30',
        ];
    }
}
