<?php

namespace App\Http\Requests;

use App\Utils\RequestPrepare;

class PaymentCheckRequest extends RequestPrepare
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|max:20',
            'order_id' => 'required'
        ];
    }
}
