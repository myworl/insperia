<?php

namespace App\Http\Requests\Course;

use App\Utils\RequestPrepare;

class CourseAddRequest extends RequestPrepare
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => ['required', 'min:3'],
            'description' => ['min:3'],
            'subject_id' => ['required', 'exists:subjects,id'],
            'order' => ['integer', 'min:1'],
            'date_start' => ['required', 'date_format:Y-m-d H:i:s'],
            'date_end' => ['required', 'date_format:Y-m-d H:i:s'],
        ];
    }
}
