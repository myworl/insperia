<?php

namespace App\Http\Requests\Course;

use App\Utils\RequestPrepare;

class CourseStoreRequest extends RequestPrepare
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|min:3",
            "description" => "min:3",
            "order" => "integer|min:1",
            "subject_id" => "required|exists:subjects,id",
            "image" => "min:3",
            "thumb" => "min:3",
            "date_start" => "date_format:Y-m-d H:i:s",
            "date_end" => "date_format:Y-m-d H:i:s",
        ];
    }
}

