<?php

namespace App\Http\Requests;

use App\Utils\RequestPrepare;

class SublessonRequest extends RequestPrepare
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sublesson_type' => 'required|string',
            'sublesson_id' => 'required|integer',
            'lesson_id' => 'required|integer|exists:App\Models\Lesson,id',
        ];
    }
}
