<?php

namespace App\Http\Requests\lesson;

use Illuminate\Foundation\Http\FormRequest;

class LessonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|string|max:200",
            "description" => "nullable|string",
            "type_id" => "nullable|integer",
            "stop_lesson_id" => "nullable|integer",
            "order" => "nullable|integer",
        ];
    }
}
