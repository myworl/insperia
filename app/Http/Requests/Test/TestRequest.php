<?php

namespace App\Http\Requests\Test;

use App\Utils\RequestPrepare;

class TestRequest extends RequestPrepare
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|string|max:195",
            "text" => "required|string|max:3000",
        ];
    }
}
