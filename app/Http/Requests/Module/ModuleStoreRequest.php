<?php

namespace App\Http\Requests\Module;

use App\Utils\RequestPrepare;

class ModuleStoreRequest extends RequestPrepare
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|min:3",
            "description" => "required|min:3",
            "order" => "required|integer|min:1",
            "open_date" => "required|date_format:Y-m-d H:i:s",
            "close_date" => "required|date_format:Y-m-d H:i:s",
            "is_additional" => "required|boolean"
        ];
    }
}
