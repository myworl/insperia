<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $gender =$this->is_male;
        if($gender !== NULL){
            if($gender === 1){
                $genderToDecode = 'male';
            }else {
                $genderToDecode = 'female';
            }
        }else{
            $genderToDecode = NULL;
        }
        $image = NULL;
        if($this->avatar != NULL){
            $image ='public/users/'.$this->avatar;
        }
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'lastName'=>$this->lastName,
            'gender'=>$genderToDecode,
            'email'=>$this->email,
            'email_verified_at'=>$this->email_verified_at,
            'avatar'=> $image,
            'phone_active'=>$this->phone_active,
            'phone'=>$this->phone,
            'instagram'=>$this->instagram,
            'telegram'=>$this->telegram,
            'birthday'=>Carbon::parse($this->birthday)->format("d.m.Y"),
            'city'=>$this->city,
            'remember_token'=>$this->remember_token,
            'role'=> RoleResource::collection($this->roles),
            'created_at'=>$this->created_at,
        ];
    }
}
