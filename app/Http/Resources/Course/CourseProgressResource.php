<?php

namespace App\Http\Resources\Course;

use App\Http\Resources\Module\ModuleProgressResource;
use App\Http\Resources\Module\ModuleResourceShort;
use App\Http\Resources\Subject\SubjectResource;
use App\Models\CourseUser;
use App\Models\UserCourseProgresse;
use Illuminate\Http\Resources\Json\JsonResource;

class CourseProgressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $user_id =auth()->user()->id;
        if(auth()->user()){
            $id_user = auth()->user()->id;
            $active_tarif = CourseUser::where('users_id', $id_user)
                ->leftJoin('course_tarif_options', 'course_tarif_options.id', '=', 'course_users.options_id')
                ->leftJoin('course_tarifs', 'course_tarifs.id', '=', 'course_tarif_options.course_tarif_id')
                ->where('courses_id', $this->id)
                ->get();
            if(count($active_tarif) > 0){
                foreach ($active_tarif as $item_tarif){
                    $result['id'] =  $item_tarif->id;
                    $result['title'] =  $item_tarif->title;
                    $result['description'] =  $item_tarif->description;
                    $result['is_subscription'] =  $item_tarif->is_subscription;
                    $result['date_end'] =  $item_tarif->date_end;
                    $result['subtitle'] =  $item_tarif->subtitle;
                    $result['type'] =  $item_tarif->type;
                }
            }
            else{
                $result = NULL;
            }
        }else{
            $result = NULL;
        }
        $curse_progresses= UserCourseProgresse::where('user_id',$user_id)->where('course_id',$this->id)->first();
        if($curse_progresses == NULL){
            $curse_progresses = new UserCourseProgresse();
            $curse_progresses->course_id = $this->id;
            $curse_progresses->user_id = $user_id;
            $curse_progresses->progress = 146;
            $curse_progresses->save();
        }
        return [
            "id" => $this->id,
            "title" => $this->title,
            "description" => $this->description,
            "order" => $this->order,
            "subject" => new SubjectResource($this->subject),
            "image" => $this->image,
            "thumb" => $this->thumb,
            "date_start" => $this->date_start,
            "date_end" => $this->date_end,
            "count_places" => $this->count_places,
            "modules" => ModuleProgressResource::collection($this->modules),
            'user_course_progresses'=> $curse_progresses,
        ];
    }
}
