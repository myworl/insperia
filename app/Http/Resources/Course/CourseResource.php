<?php

namespace App\Http\Resources\Course;

use App\Http\Resources\Module\ModuleResource;
use App\Http\Resources\Module\ModuleResourceShort;
use App\Http\Resources\Subject\SubjectResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "title" => $this->title,
            "description" => $this->description,
            "order" => $this->order,
            "subject" => new SubjectResource($this->subject),
            "image" => $this->image,
            "thumb" => $this->thumb,
            "date_start" => $this->date_start,
            "date_end" => $this->date_end,
            "count_places" => $this->count_places,
            "modules" => ModuleResourceShort::collection($this->modules),
            "knowledgebase" => CourseKnowledgeBaseResourceShort::collection($this->knowledgebase),
            "tarifs" => CourseTarifDopResource::collection($this->tarifs),
        ];
    }
}
