<?php

namespace App\Http\Resources\Course;

use App\Models\Basket;
use Illuminate\Http\Resources\Json\JsonResource;

class CourseTarifOptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */

    public function toArray($request)
    {
        $id_user = auth()->user()->id;
        return [
            'basket_id' => Basket::where('course_tarif_option_id','=', $this->id)->where('user_id','=', $id_user)->firstOrFail()->id,
            'id'=>$this->id,
            'title'=>$this->title,
            'description'=>$this->description,
            'price'=>$this->price,
            'old_price'=>$this->old_price,
            'date_end'=>$this->date_end,
            'sale_text'=>$this->sale_text,
            'is_subscription'=>$this->is_subscription,
            'tarif'=> CourseTarifResource::collection($this->tarif),
        ];
    }
}
