<?php

namespace App\Http\Resources\Course;

use Illuminate\Http\Resources\Json\JsonResource;

class CourseTarifResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
      return [
          'id'=>$this->id,
          'type'=>$this->type,
          'title'=>$this->title,
          'subtitle'=>$this->subtitle,
          'description'=>$this->description,
          'feature'=> CourseTarifFeatureResource::collection($this->feature),
          'course'=> CourseResourceShort::collection($this->course),
      ];
    }
}
