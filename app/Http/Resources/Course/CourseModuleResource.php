<?php

namespace App\Http\Resources\Course;

use App\Http\Resources\Module\ModuleAllResource;
use App\Http\Resources\Subject\SubjectResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Utils\Course;

class CourseModuleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "title" => $this->title,
            "description" => $this->description,
            "order" => $this->order,
            "image" => $this->image,
            "thumb" => $this->thumb,
            "popularity" => $this->popularity,
            "date_start" => $this->date_start,
            "date_end" => $this->date_end,
            "count_places" => $this->count_places,
            "modules" => ModuleAllResource::collection($this->courseModule),
            "subject" => new SubjectResource($this->subject),
            "tarifs" =>  Course\CourseService::courseTariff($this->id),
            "last_user_sublesson" => Course\CourseService::courseLastUserSublesson($this->id),
            "course_progress" => 146,
            "progress" => Course\CourseService::courseProgress($this->id),
        ];
    }
}
