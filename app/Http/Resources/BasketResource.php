<?php

namespace App\Http\Resources;

use App\Http\Resources\Course\CourseTarifOptionResource;
use App\Models\Basket;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class BasketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public $preserveKeys = true;
    public function toArray($request)
    {
        $id = auth()->user()->id;
        $orders_summ = Basket::where('user_id', $id)
            ->leftJoin('course_tarif_options', 'course_tarif_options.id', '=', 'baskets.course_tarif_option_id')
            ->sum('course_tarif_options.price');
        $orders_total_discount = Basket::where('user_id', $id)
            ->leftJoin('course_tarif_options', 'course_tarif_options.id', '=', 'baskets.course_tarif_option_id')
            ->sum('course_tarif_options.old_price');
        $total_discount =  $orders_total_discount-$orders_summ;

        return [

            'list' => CourseTarifOptionResource::collection($this->cart),
            'status'=>[
                'total_count'=> $this->cart->count(),
                'total_fullprice'=>(int)  $orders_summ,
                'total_discount'=>(int) $total_discount,
                'total_cost'=>(int) $orders_total_discount
            ]
        ];
    }
}
