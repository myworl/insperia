<?php

namespace App\Http\Resources\Module;

use App\Http\Resources\Lesson\LessonProgressResource;
use App\Models\UserCourseProgresse;
use Illuminate\Http\Resources\Json\JsonResource;

class ModuleLessonProgressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $user_id =auth()->user()->id;
        $curse_progresses= UserCourseProgresse::where('user_id',$user_id)->where('course_id',$this->course_id)->first();
        if($curse_progresses == NULL){
            $curse_progresses = new UserCourseProgresse();
            $curse_progresses->course_id = $this->id;
            $curse_progresses->user_id = $user_id;
            $curse_progresses->progress = 146;
            $curse_progresses->save();
        }
        return [
            "id" => $this->id,
            "name" => $this->name,
            "description" => $this->description,
            "order" => $this->order,
            "open_date" => $this->open_date,
            "close_date" => $this->close_date,
            "is_additional" => boolval($this->is_additional),
            "lesson" => LessonProgressResource::collection($this->moduleLesson),
            'user_course_progresses'=> $curse_progresses,
        ];
    }
}
