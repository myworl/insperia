<?php

namespace App\Http\Resources\Module;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Utils\Module;

class ModuleAllResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "description" => $this->description,
            "order" => $this->order,
            "open_date" => $this->open_date,
            "close_date" => $this->close_date,
            "is_additional" => boolval($this->is_additional),
            "date_deadline" => Carbon::now(),
            "is_passed" => false,
            'progress'=> Module\ModuleService::moduleProgress($this->id),
            'lesson_start'=> Module\ModuleService::moduleLessonStart($this->id),
        ];
    }
}
