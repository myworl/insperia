<?php

namespace App\Http\Resources\Module;

use App\Models\ModuleLesson;
use App\Models\UserCourseModuleProgres;
use App\Models\UserCourseProgresse;
use Illuminate\Http\Resources\Json\JsonResource;

class ModuleProgressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $user_id =auth()->user()->id;
        $user_curse_module_progresses= UserCourseModuleProgres::where('user_id',$user_id)
            ->where('course_id',$this->course_id)
            ->where('module_id',$this->id)
            ->first();
        if($user_curse_module_progresses == NULL){
            $module_lesson = ModuleLesson::where('modules_id',$this->id)
                ->with('lesson.lessonType')
                ->get()->toArray();
            foreach ($module_lesson as $module_lesson_item){
                $sample_id = $module_lesson_item['module_to_lesson'][0]['lesson_type'][0]['id'];
                if($sample_id == 2){
                    $sample_count[] = $sample_id;
                }
            }

            $user_curse_module_progresses = [
                "course_id" => $this->course_id,
                "module_id" => $this->id,
                "user_id" => $user_id,
                "lessons_current" => 0,
                "lessons_count_chc" => count($module_lesson),
                "sample_current" => 0,
                "sample_current_chc" => count($sample_count),
                "steps_current" => 0,
                "steps_current_chc" => 0,
                "progress" => 0,
            ];
        }
        return [
            "id" => $this->id,
            "name" => $this->name,
            "description" => $this->description,
            "order" => $this->order,
            "open_date" => $this->open_date,
            "close_date" => $this->close_date,
            "is_additional" => boolval($this->is_additional),
            "user_course_module_progres" => $user_curse_module_progresses,
        ];
    }
}
