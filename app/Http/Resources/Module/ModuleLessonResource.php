<?php

namespace App\Http\Resources\Module;

use App\Http\Resources\Lesson\LessonAllResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Utils\Module;

class ModuleLessonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "description" => $this->description,
            "order" => $this->order,
            "open_date" => $this->open_date,
            "close_date" => $this->close_date,
            "course_id" => $this->course_id,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "is_additional" => boolval($this->is_additional),
            "lesson" => LessonAllResource::collection($this->moduleLesson),
            'actual_probe'=> NULL,
            'last_user_sublesson'=> Module\ModuleService::moduleLastUserSublesson($this->id),
            'progress'=> Module\ModuleService::moduleProgress($this->id),
        ];
    }
}
