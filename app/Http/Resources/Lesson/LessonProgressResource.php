<?php

namespace App\Http\Resources\Lesson;

use App\Models\ModuleLesson;
use App\Models\UserCourseModuleProgres;
use App\Models\UserLessonProgress;
use Illuminate\Http\Resources\Json\JsonResource;

class LessonProgressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $user_id =auth()->user()->id;
        $user_lesson_progress= UserLessonProgress::where('user_id',$user_id)
            ->where('lesson_id',$this->id)
            ->first();
        if($user_lesson_progress == NULL){

            $lesson = ModuleLesson::where('lessons_id',$this->id)
                ->with('lesson.lessonVideo','lesson.lessonWebinar')
                ->get()->toArray();
            foreach ($lesson as $lesson_item){
                $video = $lesson_item['module_to_lesson'][0]['lesson_video'];
                $webinar = $lesson_item['module_to_lesson'][0]['lesson_webinar'];
            }
            $user_lesson_progress = [
                "lesson_id" => $this->id,
                "user_id" => $user_id,
                "videos" => 0,
                "videos_count_chc" => count($video),
                "webinars" => 0,
                "webinars_count_chc" => count($webinar),
                "tasks" => 0,
                "tasks_count_chc" => 0,
                "steps" => 0,
                "steps_count_chc" => 0,
                "progress" => 0,
            ];
        }
        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'description'=>$this->description,
            'stop_lesson_id'=>$this->stop_lesson_id,
            'order'=>$this->order,
            'updated_at'=>$this->updated_at,
            'user_lesson_progress'=>$user_lesson_progress,
//            'type'=> LessontypeResource::collection($this->lessonType),
//            'webinar'=> LessonWebinarResource::collection($this->lessonWebinar),
//            'video'=> LessonVideoResource::collection($this->lessonVideo),
        ];
    }
}
