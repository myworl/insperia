<?php

namespace App\Http\Resources\Lesson;

use App\Models\LessonType;
use Illuminate\Http\Resources\Json\JsonResource;

class LessonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'description'=>$this->description,
            'stop_lesson_id'=>$this->stop_lesson_id,
            'order'=>$this->order,
            'updated_at'=>$this->updated_at,
            'type'=> LessontypeResource::make($this->lessonType),
            'webinar'=> LessonWebinarResource::collection($this->lessonWebinar),
            'video'=> LessonVideoResource::collection($this->lessonVideo),
        ];
    }
}
