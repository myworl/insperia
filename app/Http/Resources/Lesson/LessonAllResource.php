<?php

namespace App\Http\Resources\Lesson;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Utils\Lesson;

class LessonAllResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'description'=>$this->description,
            'stop_lesson_id'=>$this->stop_lesson_id,
            'order'=>$this->order,
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at,
            'is_passed'=>Lesson\LessonService::isPasset($this->id),
            'is_additional'=> true,
            'date_start'=>Carbon::now(),
            'date_end'=>Carbon::now(),
            'date_deadline'=>Carbon::now(),
            'lesson_type'=> new LessontypeResource($this->lessonType),
            'progress' => Lesson\LessonService::LessonProgress($this->id),
            'last_user_sublesson' => Lesson\LessonService::lastUserSublesson($this->id),
        ];
    }
}
