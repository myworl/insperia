<?php 

namespace App\Utils;

use App\Models\UserLessonProgress;
use App\Utils\Course\CourseService;
use App\Models\Lesson;
use App\Utils\Module\ModuleService;
use Carbon\Carbon;

class ProgressService {

    public static function setProgressLimits($user_id, $course_id = null, $module_id = null, $force = false)
    {
        if (null !== $course_id)
        {
            $get_lessons = CourseService::courseLessons($course_id);
        }elseif (null !== $module_id){
            $get_lessons = ModuleService::moduleLessonIds($module_id);
        }
        
        if (null === $get_lessons || (null === $course_id && null === $module_id))
        {
            return null;
        }

        if (true !== $force)
        {
            $lessons_exists_progress = UserLessonProgress::whereIn('lesson_id', $get_lessons)->where('user_id', $user_id)->pluck('lesson_id');
            $course_lessons = $get_lessons->diff($lessons_exists_progress);
        }

        $lessons = Lesson::whereIn('id', $course_lessons)->withCount('lessonWebinar', 'lessonVideo', 'lessonTest')->get();
        $progress_insert_array = [];

        foreach ($lessons as $lesson)
        {
            $progress_insert_array[] = [
                'videos' => 0,
                'webinars' => 0,
                'tasks' => 0,
                'steps' => 0,
                'progress' => 0,
                'videos_count_chc' => $lesson->lesson_webinar_count,
                'webinars_count_chc' => $lesson->lesson_webinar_count,
                'tasks_count_chc' => 0,
                'steps_count_chc' => $lesson->lesson_test_count,
                'lesson_id' => $lesson->id,
                'user_id' => $user_id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        }

        if ($lessons->count() > 0)
        {
            UserLessonProgress::insert($progress_insert_array);
        }

        return $get_lessons;
    }

}