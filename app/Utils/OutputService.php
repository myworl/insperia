<?php

namespace App\Utils;

class OutputService
{

    const CODE_OK = 0;
    const CODE_NOT_AUTH = 1;
    const CODE_ACCESS_DENY = 2;
    const CODE_UNKNOWN = 100;
    const CODE_NOT_FOUND = 404;
    const CODE_UNPROCESSABLE_ENTITY = 422;

    public static function sendJson($data = [], $code = self::CODE_OK)
    {
        return response()->json([
            "code" => $code,
            "data" => $data
        ]);
    }
}
