<?php 

namespace App\Utils;

use App\Models\OrderList;
use App\Utils\Payment\PaymentInterface;
use App\Models\Transaction;
use Exception;

class Payment {

    protected $payment_class;
    protected $payment_name;

    public function __construct($payment_name = null)
    {
        $this->setPayment($payment_name);
    }

    public function setPayment($payment_name = null)
    {
        if (null === $payment_name)
        {
            $this->payment_name = config('payment.default_payment');
        }else{
            $this->payment_name = $payment_name;
        }

        $payment_class_string = "\\App\\Utils\\Payment\\Repository\\".ucfirst($this->payment_name);
        $this->payment_class = new $payment_class_string;
        $this->payment_name = $payment_name;
    }

    public function getPayment()
    {
        return $this->payment_class;
    }

    public function create(OrderList $order_list){

        if (!($this->payment_class instanceof PaymentInterface)) {
            throw new Exception('Класс оплаты не определен');
        }

        if ($this->payment_class->create($order_list))
        {
            
            $order_list->payment_type = $this->payment_name;
            $order_list->save();

            $transaction = new Transaction();
            $transaction->transaction = $this->payment_class->getTransactionId();
            $transaction->orders_list_id  = $order_list->id;
            $transaction->link  = $this->payment_class->getRedirectUrl();
            $transaction->payment_status  = Transaction::STATUS_WAITING;
            $transaction->save();

            $order_list->transaction_id = $transaction->id;
            $order_list->save();
        }

        return true;
    }

    public function getRedirectUrl(){
        return $this->payment_class->getRedirectUrl();
    }

    public function check(Transaction $transaction)
    {
        if (!($this->payment_class instanceof PaymentInterface)) {
            throw new Exception('Класс оплаты не определен');
        }

        return $this->payment_class->check($transaction);
    }
}