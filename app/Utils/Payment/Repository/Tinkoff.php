<?php 

namespace App\Utils\Payment\Repository;

use App\Models\OrdersPosition;
use App\Utils\Payment\PaymentInterface;
use App\Models\OrderList;
use App\Models\User;
use Exception;
use App\Models\Transaction;
use Illuminate\Support\Facades\Http;

class Tinkoff implements PaymentInterface {

    public function create(OrderList $order) : bool
    {
        $user = User::find($order->user_id);
        $order_position = OrdersPosition::where('orders_lists_id',$order->id)->with('ordersPossininToOption')->get();
        foreach ($order_position as $order_position_item){
            $items[] = [
                'Name' => $order_position_item['ordersPossininToOption'][0]['title'],
                'Quantity' => 1.00,
                'Amount' => $order_position_item->price * 100,
                'Price' => $order_position_item->price * 100,
                'PaymentObject' => 'payment',
                'Tax' => 'none',
            ];
        }

        $receipt = [
            'Email'      => $user->email,
            'Phone'      => $user->phone,
            'Taxation'   => 'usn_income',
            'Items'   => $items,
            'FfdVersion'  => "FfdVersion: 1.05"
        ];
        $payment_data = [
            'TerminalKey' => config('payment.options.tinkoff.tinkoff_terminal'),
            'Token' => config('payment.options.tinkoff.tinkoff_secret_key'),
            'OrderId'       => $order->order_id, 
            'Amount'        => $order->cost,         
            'Description'   => 'Покупка курсов',   
            'Email'         => $user->email,  
            'Phone'         => $user->phone,  
            'Name'          => $user->name, 
            'Taxation'      => 'usn_income',
            'SuccessURL' => config('app.url').'/api/payment/check?type=tinkoff&order_id='.$order->order_id,
            'FailURL' => config('app.url').'/api/payment/check?type=tinkoff&order_id='.$order->order_id,
            'Receipt' => $receipt,
        ];

        $response = Http::accept('application/json')->post($this->getUrl('Init'), $payment_data)->getBody()->getContents();
        $response = json_decode($response, true);

        if (1 !== $response['Success'] && $response['ErrorCode'] > 0)
        {
            throw new Exception('Bank error: ['.$response['ErrorCode']. '] '.$response['Message']);
        }

        if (null !== $response['PaymentURL'])
        {
            $this->redirectUrl = $response['PaymentURL'];
        }

        if (null !== $response['PaymentId'])
        {
            $this->transaction_id = $response['PaymentId'];
        }

        return true;
    }

    public function getUrl($type)
    {
        $url = config('payment.options.tinkoff.tinkoff_url');
        $url .= $type;

        return $url;
    }

    
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }


    public function getTransactionId()
    {
        return $this->transaction_id;
    }

    public function check(Transaction $transaction) : string
    {

        $payment_data = [
            'TerminalKey' => config('payment.options.tinkoff.tinkoff_terminal'),
            'Token' => config('payment.options.tinkoff.tinkoff_secret_key'),
            'PaymentId' => $transaction->transaction
        ];
        $response = Http::accept('application/json')->post($this->getUrl('GetState'), $payment_data)->getBody()->getContents();
        $response = json_decode($response, true);

        if (isset($response['Status']) && $response['Status'] == 'CONFIRMED')
        {
            return Transaction::STATUS_APPROVED;
        }else{
            return Transaction::STATUS_REJECTED;
        }

    }
}