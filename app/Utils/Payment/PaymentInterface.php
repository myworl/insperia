<?php 

namespace App\Utils\Payment;

use App\Models\OrderList;
use App\Models\Transaction;

interface PaymentInterface 
{
    public function create(OrderList $order) : bool;
    public function check(Transaction $transaction) : string;
    public function getRedirectUrl();
    public function getTransactionId();
}