<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    const STATUS_WAITING = 'wait';
    const STATUS_APPROVED = 'approved';
    const STATUS_REJECTED = 'rejected';
}
