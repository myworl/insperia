<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrdersPosition extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $fillable = [
        'orders_lists_id',
        'course_tarif_options_id',
    ];
    public function ordersPossininToOption()
    {
        return $this->hasMany(CourseTarifOption::class,'id','course_tarif_options_id' );
    }
}
