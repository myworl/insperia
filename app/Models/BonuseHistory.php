<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BonuseHistory extends Model
{
    use HasFactory;

    protected $table = 'bonuse_histories';
}
