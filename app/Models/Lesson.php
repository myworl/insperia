<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Test;

class Lesson extends Model
{
    use HasFactory;

    public function lessonType()
    {
        return $this->hasOne(LessonType::class, 'id','type_id');
    }

    public function lessonWebinar()
    {
        return $this->belongsToMany(LessonWebinar::class, 'pivot_lesson_webinar','lesson_id','webinar_id');
    }

    public function lessonVideo()
    {
        return $this->belongsToMany(LessonVideo::class, 'pivot_lesson_video','lesson_id','video_id');
    }

    public function lessonTest()
    {
        return $this->hasOne(Test::class, 'lesson_id','id');
    }
}
