<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseTarif extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'course_tarifs';

    public function feature()
    {
        return $this->hasMany(CourseTarifFeature::class, 'course_tarif_id','id');
    }
    public function course()
    {
        return $this->belongsToMany(Course::class, 'course_tarifs','id','courses_id');
    }
    public function option()
    {
        return $this->hasMany(CourseTarifOption::class, 'course_tarif_id','id');
    }
    public function tarifToCourse()
    {
        return $this->belongsToMany(Course::class, 'course_tarifs','id','courses_id');
    }



}
