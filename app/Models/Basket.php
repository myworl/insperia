<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     title="Cart",
 *     description="cart model",
 *     @OA\Xml(
 *         name="cart"
 *     )
 * )
 */
class Basket extends Model
{
    use HasFactory;

    public function option() {
        return $this->hasMany(CourseTarifOption::class,'course_tarif_id','id');
    }
    public function tarif() {
        return $this->hasMany(CourseTarif::class,'course_id','id');
    }

    public function baskToOption() {
        return $this->hasOne(CourseTarifOption::class,'id','course_tarif_option_id');
    }
}
