<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    use HasFactory;

    protected $fillable = ["name", "description", "order", "open_date", "close_date", "is_additional"];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
    public function moduleLesson()
    {
        return $this->belongsToMany(Lesson::class, 'module_lessons','modules_id','lessons_id');
    }
}
