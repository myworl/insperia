<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LessonType extends Model
{
    use HasFactory;

    const TYPE_DEFAULT = 1;
    const TYPE_DEMO = 2;


    const TYPE_DESCRIPTION = [
        self::TYPE_DEFAULT => "lesson",
        self::TYPE_DEMO =>  "probe",
    ];
}
