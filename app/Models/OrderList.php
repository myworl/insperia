<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderList extends Model
{
    protected $table = 'orders_lists';

    const STATUS_NEW = "new";
}
