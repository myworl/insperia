<?php

namespace App\Models;

use App\Models\CourseTarif;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class CourseTarifOption extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function tarif()
    {
        return $this->belongsToMany(CourseTarif::class, 'course_tarif_options','id','course_tarif_id');
    }

    public function optionToTarif()
    {
        return $this->belongsToMany(CourseTarif::class, 'course_tarif_options','id','course_tarif_id');
    }

}
