<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseUser extends Model
{
    use HasFactory;

    protected $fillable = [
        'users_id',
        'options_id',
    ];

    public function courseUsers()
    {
        return $this->belongsTo(CourseTarifOption::class, 'options_id','id');
    }
    public function courseUsersToOption()
    {
        return $this->belongsToMany(CourseTarifOption::class, 'course_users','id','options_id');
    }
}
