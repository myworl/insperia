<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModuleLesson extends Model
{

    public function lesson() {
        return $this->hasOne(Lesson::class,'id','lessons_id');
    }

}
