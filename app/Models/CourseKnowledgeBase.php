<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseKnowledgeBase extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["section", "question", "answer", "order"];


    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
