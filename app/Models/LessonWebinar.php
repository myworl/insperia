<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LessonWebinar extends Model
{
    use HasFactory;

    public function UserSublessonWebinar()
    {
        return $this->hasOne(UserSublessonWebinar::class, 'sublesson_webinar_id', 'id');
    }
}
