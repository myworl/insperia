<?php

namespace App\Models;

use App\Traits\HasRolesAndPermissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;


/**
 * @OA\Schema(
 *     title="User",
 *     description="User model",
 *     @OA\Xml(
 *         name="User"
 *     )
 * )
 */
class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, HasRolesAndPermissions;
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Связь "Многие ко многим".
     *
     * @return array
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'users_roles', 'user_id', 'role_id');
    }
    /**
     * Связь "Один ко многим".
     *
     * @return array
     */
    public function avatars()
    {
        return $this->hasMany(Avatar::class);
    }

    public function courses()
    {
        return $this->belongsToMany(Course::class);
    }


    public function cart()
    {
        return $this->belongsToMany(CourseTarifOption::class, 'baskets', 'user_id', 'course_tarif_option_id');
    }

        /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    protected static function boot()
    {
        parent::boot();

        static::updating(function ($model) {
            $oldData = $model->getOriginal();
            $newData = $model->getAttributes();

            if ($oldData['bonuses'] !== $newData['bonuses'])
            {
                $diff = $oldData['bonuses'] - $newData['bonuses'];
                $history = new BonuseHistory();
                $history->user_id = $newData['id'];
                $history->bonuses = abs($diff);
                $history->status = $oldData['bonuses'] < $newData['bonuses'] ? 'withdrawal' : 'replenishment';
                $history->save();
            }

            if ($oldData['balance'] !== $newData['balance'])
            {
                $diff = $oldData['balance'] - $newData['balance'];
                $history = new BalanceHistory();
                $history->user_id = $newData['id'];
                $history->balance = abs($diff);
                $history->status = $oldData['balance'] < $newData['balance'] ? 'withdrawal' : 'replenishment';
                $history->save();
            }
        });
    }
}
