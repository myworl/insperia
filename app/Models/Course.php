<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $fillable = ["title", "description", "subject_id", "order", "image", "thumb", "date_start", "date_end"];

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function modules()
    {
        return $this->hasMany(Module::class)->orderBy("order");
    }

    public function knowledgebase()
    {
        return $this->hasMany(CourseKnowledgeBase::class);
    }

    public function tarifs()
    {
        return $this->hasMany(CourseTarif::class, 'courses_id',  'id');
    }

    public function courseToSubject()
    {
        return $this->belongsToMany(Subject::class, 'courses','id','subject_id');
    }

    public function tarifToCourse()
    {
        return $this->belongsToMany(CourseTarif::class, 'course_tarifs','courses_id','id');
    }

    public function courseModule()
    {
        return $this->belongsToMany(Module::class, 'course_modules','course_id','module_id');
    }

}
