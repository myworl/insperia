<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseModule extends Model
{
    use HasFactory;

    public function courseModules()
    {
        return $this->hasMany(Module::class, 'id','module_id');
    }

}
