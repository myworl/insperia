<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSublessonWebinar extends Model
{
    use HasFactory;
    public function sublessonWebinar()
    {
        return $this->hasMany(LessonWebinar::class, 'id','sublesson_webinar_id');
    }
}
