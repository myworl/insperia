<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controller\BasketController;
use App\Utils\ProgressService;
use App\Utils\UserService;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/403', function () {
    return view('403');
});
Route::get('/404', function () {
    return view('404');
});

Route::get('/re', function () {
    return  '545';
});


Route::get('/dd', function() {
    dd(ProgressService::setProgressLimits(1, 1));
});
