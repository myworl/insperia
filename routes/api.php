<?php

use App\Http\Controllers\CourseModuleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UpdatePasswordController;
use App\Http\Controllers\SmsController;
use App\Http\Controllers\AvatarController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BasketController;
use App\Http\Controllers\OrderListController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ModuleLessonController;
use App\Http\Controllers\Lesson\LessonController;
use App\Http\Controllers\Lesson\LessonWebinarController;
use App\Http\Controllers\Lesson\LessonVideoController;
use App\Http\Controllers\SublessonController;
use App\Http\Controllers\BonusesController;
use App\Http\Controllers\BalanceController;
use App\Http\Controllers\BasketUnregController;
use App\Http\Controllers\Test\TestController;
use App\Http\Controllers\Test\TestQuestionController;

Route::controller(AuthController::class)->group(function () {
    Route::post('/auth/', 'login')->middleware('auth:api');
    Route::post('/auth/register','registration');
    Route::post('/auth/logout',  'logout');
    Route::post('/auth/token/refresh',  'refresh');
    Route::post('/auth/me',  'me')->middleware('auth');
    Route::post('/auth/update', 'update')->middleware('auth');
    //Email подтверждение
    Route::post('/profile/email/verify/send',  'storeMail')->middleware('auth:api');
    Route::post('/profile/email/verify',  'addVerificationMail')->middleware('auth:api');
});

//Востановить пароль
Route::controller(UpdatePasswordController::class)->group(function () {
    Route::post('/auth/forgot/password', 'userForgotPassword');
    Route::post('/auth/reset/password', 'userResetPassword');
});

Route::group(['middleware' => 'auth:api'], function () {
    //СМС подтверждение
    Route::controller(SmsController::class)->group(function () {
        Route::post('/profile/phone/verify/send/', 'smsSend');
        Route::post('/profile/phone/verify/', 'smsVerification');
    });

    //Аватар
    Route::controller(AvatarController::class)->group(function () {
        Route::post('/avatar/store/',  'avatarAdd');
        Route::post('/avatar/destroy/', 'avatarDelete');
    });

    //Дополнительные страницы
    Route::controller(PageController::class)->group(function () {
        Route::get('/page/{page_id}',  'pageShowId');
        Route::post('/page/store/',  'pagePositionAdd');
        Route::post('/page/update/{page_id}', 'pageUpdateId');
        Route::delete('/page/destroy/{page_id}','pageDestroy');
    });

    //Пользователь для АДМИНКИ
    Route::controller(UserController::class)->group(function () {
        Route::get('/profile',  'userProfilesDumpAll');
        Route::post('/profile/store',  'userProfileStore');
        Route::get('/profile/{id}',  'userProfileShowById');
        Route::post('/profile/update/{id}', 'userProfileUpdate');
        Route::delete('/profile/destroy/{id}', 'userProfileDestroy');
    });

    // Корзина
    Route::controller(BasketController::class)->group(function () {
        Route::get('/cart/',  'basketShowAll');
        Route::post('/cart/product/add/{id}','basketPositionAdd')->where(['id' => '[0-9]+']);
        Route::post('/cart/product/remove/{id}', 'basketPositionRemove')->where(['id' => '[0-9]+']);
        Route::post('/cart/clear/','basketClear');
        Route::get('/cart/status/','status');
    });

    //Оформление заказа | частично устарело, функционал переехал в /payment (PaymentController), удалить по завершению доработки /payment
    Route::post('/order/add', [App\Http\Controllers\PaymentController::class, 'create']);

    Route::controller(OrderListController::class)->group(function () {
        Route::post('/order/index', 'paymentShowAll');
        Route::get('/order/get/{payment_id}','paymentGetId')->where(['payment_id' => '[0-9]+']);
        Route::post('/order/remove/{payment_id}', 'paymentDeleteId')->where(['payment_id' => '[0-9]+']);
        Route::get('/order/status/{payment_id}',  'paymentStatusId')->where(['payment_id' => '[0-9]+']);
        Route::post('/order/status/change/{payment_id}/{id_status}', 'paymentStatusChangeId')->where(['payment_id' => '[0-9]+']);
    });

    //Предметы
    Route::controller(SubjectController::class)->group(function () {
        Route::post('/subject', 'subjectStore');
        Route::patch('/subject/{subject}', 'subjectUpdateId');
    });

    //Курсы, Модули, База знаний
    Route::controller(CourseController::class)->group(function () {
        //Курсы
        Route::get('/my_courses', 'myCoursesGetAll');
        Route::get('/my_courses/{course}', 'myCoursesGetById');
        Route::post('/course', 'coursePositionAdd');
        Route::patch('/course/{course}', 'courseUpdateById');
        Route::get('/course_progress','courseProgressShowById');

        //База знаний
        Route::post('/course/{course}/knowledgebase', 'createAndAddNewKnowledgeBase');
        Route::patch('/course/{course}/knowledgebase/{knowledgebase}', 'updateKnowledgeBase');
    });

    //Модули
    Route::controller(ModuleController::class)->group(function () {
        Route::post('/course/{course}/module', 'createAndAddNewModule');
        Route::patch('/course/{course}/module/{module}','updateModule');
        Route::get('/module_progress', 'moduleProgressGetById');

        Route::post('/module/remove/{id}', 'deleteModuleById');
    });

    Route::controller(SublessonController::class)->group(function () {
        Route::post('/user_sublesson_video_done',  'videoSublessonAddById');
        Route::post('/user_sublesson_webinar_done', 'webinarSublessonAddDyId');
        Route::post('/sublesson', 'sublessonGetInfo');
    });

    //Уроки модуля
    Route::controller(ModuleLessonController::class)->group(function () {
        Route::get('/module_lessons',  'moduleLessonGet');
        Route::post('/module_lesson/create',  'moduleLessonCreate');
        Route::post('/module_lesson/delete',  'moduleLessonDelete');
    });
});

// Оплата заказа
Route::controller(PaymentController::class)->group(function () {
    Route::post('/payment/create', 'create')->middleware('auth:api');
    Route::get('/payment/check', 'check');
});

//Модули
Route::controller(ModuleController::class)->group(function () {
    Route::get('/course/module',  'moduleGetById');
});


//Допродажа
Route::get('/cart/upsale', [BasketController::class, 'upsaleShowAll']);

//Предметы
Route::controller(SubjectController::class)->group(function () {
    Route::get('/subject', 'subjectShowAll');
    Route::get('/subject/{subject}', 'subjectShowId');
});

//Курсы
Route::controller(CourseController::class)->group(function () {
    Route::get('/course', 'courseShowAll');
    Route::get('/course/{course}','courseShowById');
    //Route::get('/course_modules','courseModuleGet');

    Route::post('/course', 'createCourse');
    Route::post('/course/{id}', 'deleteCourseById');
});

Route::get('/course_modules', [CourseModuleController::class, 'courseModuleGet']);

//Урок
Route::controller(LessonController::class)->group(function () {
    Route::get('/lesson', 'lessonGetById');
    Route::post('/lesson_create', 'lessonCreate');
    Route::post('/lesson_update', 'lessonUpdateById');
    Route::post('/lesson_delete', 'lessonDeleteById');
});

//Вебинар
Route::controller(LessonWebinarController::class)->group(function () {
    Route::get('/lesson/webinar', 'webinarGetById');
    Route::post('/lesson/webinar_create', 'webinarAdd');
    Route::post('/lesson/webinar_update', 'webinarUpdateShowById');
    Route::post('/lesson/webinar_delete','webinarDeleteById');
});

//Видеоурок
Route::controller(LessonVideoController::class)->group(function () {
    Route::get('/lesson/video',  'videoGetById');
    Route::post('/lesson/video_create', 'videoAdd');
    Route::post('/lesson/video_update', 'videoUpdateShowById');
    Route::post('/lesson/video_delete', 'videoDeleteById');
});

//Бонусы
Route::controller(BonusesController::class)->group(function () {
    Route::get('/user_bonuses/history','getHistory');
    Route::get('/user_bonuses/history/{id}','getHistoryById');
    Route::post('/user_bonuses_plus','bonusesUpdateById');
    Route::post('/user_bonuses_set', 'bonusesUpdateSetById');
});

//Баланс
Route::controller(BalanceController::class)->group(function () {
    Route::get('/user_balance/history','getHistory');
    Route::get('/user_balance/history/{id}','getHistoryById');
    Route::post('/user_balance_plus','balanceUpdatePlusById');
    Route::post('/user_balance_set','balanceUpdateSetById');
});

Route::post('/basket_unreg', [BasketUnregController::class, 'basketUnregGetAll']);

Route::controller(TestController::class)->group(function () {
    Route::post('/test','create');
    Route::post('/test/update','update');
    Route::delete('/test','delete');

    Route::post('/get_lesson_test','get');
});

Route::controller(TestQuestionController::class)->group(function () {
    Route::get('/test/question/types', 'types');
    Route::post('/test/question/check', 'check');
    Route::post('/test/question','create');
    Route::post('/test/question/update', 'update');
    Route::delete('/test/question', 'delete');
});

    //ТЕСТОВЫЙ роут
    //Route::get('/dd', [App\Http\Controllers\TestController::class, 'test']);