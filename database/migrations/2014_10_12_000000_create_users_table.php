<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('lastName')->nullable();
            $table->string('gender')->nullable();
            $table->string('email')->unique();
            $table->string('instagram')->nullable();
            $table->string('telegram')->nullable();
            $table->string('vk')->nullable();
            $table->string('vkFirstName')->nullable();
            $table->string('vkLastName')->nullable();
            $table->string('timezone')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('avatar')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone_active')->nullable();
            $table->string('phoneTwo')->nullable();
            $table->string('birthday')->nullable();
            $table->string('city')->nullable();
            $table->rememberToken();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
