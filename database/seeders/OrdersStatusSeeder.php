<?php

namespace Database\Seeders;

use App\Models\OrdersStatus;
use Illuminate\Database\Seeder;

class OrdersStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrdersStatus::truncate();

        $orders_status= [
            [
                "name" => "NEW",
            ],
            [
                "name" => "NOT PAID",
            ],
            [
                "name" => "PAID",
            ],
            [
                "name" => "COMPLETED",
            ]

        ];

        foreach ($orders_status as $status) {
            OrdersStatus::create($status);
        }
    }
}
