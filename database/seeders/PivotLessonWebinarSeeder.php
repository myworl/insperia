<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PivotLessonWebinarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($lesson_id = 1; $lesson_id <= 24; $lesson_id++) {
            for ($webinar_id = 1; $webinar_id <= 16; $webinar_id++) {

                $data[] = [
                    'lesson_id' => $lesson_id,
                    'webinar_id' => $webinar_id
                ];
            }
        }

        DB::table('pivot_lesson_webinar')->insert($data);
    }
}
