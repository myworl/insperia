<?php

namespace Database\Seeders;

use App\Models\Lesson;
use Illuminate\Database\Seeder;

class LessonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Lesson::truncate();

        for($module = 1; $module < 13; $module++)
        {
            $lessons[] = [
                "title" => "Урок: Изучение главы #".($module*2-1),
                "description" => "Что такое степень сравнения прилагательных? Как склонять числительные? Ответы на эти и другие вопросы очень подробно, понятно даны в уроке.",
                "type_id" => 1,
                "stop_lesson_id" => 0,
                "order" => 0,
            ];

            $lessons[] = [
                "title" => "Урок: Изучение главы #".($module*2),
                "description" => "Правила, которые необходимо знать, чтобы правильно расставить все запятые. Причастный и деепричастный оборот",
                "type_id" => 1,
                "stop_lesson_id" => 0,
                "order" => 0,
            ];
        }

        foreach ($lessons as $value) {
            Lesson::create($value);
        }
    }
}
