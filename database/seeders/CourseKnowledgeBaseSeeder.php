<?php

namespace Database\Seeders;

use App\Models\CourseKnowledgeBase;
use Illuminate\Database\Seeder;

class CourseKnowledgeBaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lists = [
            [
                "course_id" => 1,
                "section" => "Test 1",
                "question" => "Question 1",
                "answer" => "Answer 1",
                "order" => 1
            ],
            [
                "course_id" => 1,
                "section" => "Test 2",
                "question" => "Question 2",
                "answer" => "Answer 2",
                "order" => 2
            ],
            [
                "course_id" => 2,
                "section" => "Test 3",
                "question" => "Question 1",
                "answer" => "Answer 1",
                "order" => 1
            ],
            [
                "course_id" => 3,
                "section" => "Test 4",
                "question" => "Question 2",
                "answer" => "Answer 2",
                "order" => 2
            ],
            [
            "course_id" => 4,
            "section" => "Test 3",
            "question" => "Question 1",
            "answer" => "Answer 1",
            "order" => 1
            ],
            [
                "course_id" => 5,
                "section" => "Test 4",
                "question" => "Question 2",
                "answer" => "Answer 2",
                "order" => 2
            ]
        ];

        CourseKnowledgeBase::truncate();

        foreach ($lists as $list) {
            CourseKnowledgeBase::create($list);
        }
    }
}
