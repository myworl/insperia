<?php

namespace Database\Seeders;

use App\Models\LessonVideo;
use Illuminate\Database\Seeder;

class LessonVideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LessonVideo::truncate();

        for($lesson = 1; $lesson < 27; $lesson++)
        {
            $lesson_videos = [
                [
                    "title" => "Название видеоурока с id".($lesson*6-5),
                    "iframeEmbed" => "<iframe src='https://www.youtube.com/embed/ClUASZ6JHsE'></iframe>",
                    "text" => 'Слова с приставками (включая вне-, nосле-, сверх-, а-, анти-, архи-, инфра-, контр-, ультра- и др.)',
                ],
                [
                    "title" => "Название видеоурока с id".($lesson*6-4),
                    "iframeEmbed" => "<iframe src='https://www.youtube.com/embed/ClUASZ6JHsE'></iframe>",
                    "text" => 'Слова с приставками (включая вне-, nосле-, сверх-, а-, анти-, архи-, инфра-, контр-, ультра- и др.)',
                ],
                [
                    "title" => "Название видеоурока с id".($lesson*6-3),
                    "iframeEmbed" => "<iframe src='https://www.youtube.com/embed/ClUASZ6JHsE'></iframe>",
                    "text" => 'Слова с приставками',
                ],
                [
                    "title" => "Название видеоурока с id".($lesson*6-2),
                    "iframeEmbed" => "<iframe src='https://www.youtube.com/embed/ClUASZ6JHsE'></iframe>",
                    "text" => 'Слова с приставками (включая вне-, nосле-, сверх-, а-, анти-, архи-, инфра-, контр-, ультра- и др.)',
                ],
                [
                    "title" => "Название видеоурока с id".($lesson*6-1),
                    "iframeEmbed" => "<iframe src='https://www.youtube.com/embed/ClUASZ6JHsE'></iframe>",
                    "text" => 'Слова с приставками (включая вне-, nосле-, сверх-, а-, анти-, архи-, инфра-, контр-, ультра- и др.)',
                ],
                [
                    "title" => "Название видеоурока с id".($lesson*6),
                    "iframeEmbed" => "<iframe src='https://www.youtube.com/embed/ClUASZ6JHsE'></iframe>",
                    "text" => 'Слова с приставками (включая вне-, nосле-, сверх-, а-, анти-, архи-, инфра-, контр-, ультра- и др.)',
                ]
            ];

            foreach ($lesson_videos as $video_item) {
                LessonVideo::create($video_item);
            }
        }
    }
}
