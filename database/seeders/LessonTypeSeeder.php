<?php

namespace Database\Seeders;

use App\Models\LessonType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LessonTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lesson_type= [
            [
                "title" => "Урок",
            ],
            [
                "title" => "Пробник",
            ],

        ];

        foreach ($lesson_type as $lesson_type_item) {
            LessonType::create($lesson_type_item);
        }
    }
}
