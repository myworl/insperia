<?php

namespace Database\Seeders;

use App\Models\Subject;
use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subjects = [
            [
                "name" => "Информатика",
                "description" => "",
                "background" => "#B199F0",
                "order" => 1,
            ],
            [
                "name" => "Математика",
                "description" => "",
                "background" => "#FC8F55",
                "order" => 2,
            ],
            [
                "name" => "Русский язык",
                "description" => "",
                "background" => "#65EBBB",
                "order" => 3,
            ],
            [
                "name" => "Физика",
                "description" => "",
                "background" => "#FCFF5E",
                "order" => 3,
            ],
        ];

        Subject::truncate();

        foreach ($subjects as $subject) {
            Subject::create($subject);
        }
    }
}
