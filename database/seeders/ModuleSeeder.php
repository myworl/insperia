<?php

namespace Database\Seeders;

use App\Models\Module;
use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Module::truncate();

        for($i = 1; $i < 5; $i++)
        {
            $modules[] = [
                "name" => "Название модуля",
                "description" => "Основные уроки курса с записями вебинаров, видеоуроками и тестовыми заданиями будут добавлены здесь",
                "order" => 1,
                "open_date" => "2022-03-10 08:00:00",
                "close_date" => "2022-04-10 20:00:00",
                "course_id" => $i,
                "is_additional" => false,
            ];

            $modules[] = [
                "name" => "Название модуля",
                "description" => "Уроки с тестовой практикой",
                "order" => 2,
                "open_date" => "2022-03-10 12:00:00",
                "close_date" => "2022-05-01 18:00:00",
                "course_id" => $i,
                "is_additional" => false,
            ];

            $modules[] = [
                "name" => "Название модуля",
                "description" => "Основные уроки курса с записями видеоуроклм и тестовыми заданиями лежат здесь",
                "order" => 3,
                "open_date" => "2022-03-10 08:00:00",
                "close_date" => "2022-04-10 20:00:00",
                "course_id" => $i,
                "is_additional" => true,
            ];
        }

        foreach ($modules as $module) {
            Module::create($module);
        }
    }
}
