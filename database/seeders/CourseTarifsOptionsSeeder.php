<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CourseTarifOption;

class CourseTarifsOptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        
        for($tarif = 1; $tarif < 13; $tarif++)
        {
            $price = 7000 - rand(0,4) * 500;
            $old_price = $price + rand(0,1) * 500;

            $tarif_options[] = [
                "title" => 'Первая опция (До 12.11.2022)',
                "description" => "Без проверок",
                "price" => $price,
                "old_price" => $old_price,
                "date_end" => "2022-09-10 08:00:00",
                "course_tarif_id" => $tarif,
            ];

            $tarif_options[] = [
                "title" => 'Вторая опция (До 12.11.2022)',
                "description" => "Без проверок",
                "price" => $price-1500,
                "old_price" => $old_price+500,
                "date_end" => "2022-09-10 08:00:00",
                "course_tarif_id" => $tarif,
            ];
        }

        foreach ($tarif_options as $value) {
            CourseTarifOption::create($value);
        }
    }
}
