<?php

namespace Database\Seeders;

use App\Models\CourseModule;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CourseModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 8; $i++) {
            $couse_modules[] =
                [
                    "course_id" => ceil( $i / 2),
                    "module_id" => $i,
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now(),
                ];
        }
        foreach ($couse_modules as $couse_m) {
            CourseModule::create($couse_m);
        }
    }
}
