<?php

namespace Database\Seeders;

use App\Models\LessonWebinar;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LessonWebinarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LessonWebinar::truncate();

        for($lesson = 1; $lesson < 27; $lesson++)
        {
            $lesson_webinars = [
                [
                    "title" => "Название видеоурока с id".($lesson*6-5),
                    "webinarLink" => "<iframe src='https://www.youtube.com/embed/ClUASZ6JHsE'></iframe>",
                    "date" => '2022-07-10 08:00:00',
                    "text" => 'Слова с приставками (включая вне-, nосле-, сверх-, а-, анти-, архи-, инфра-, контр-, ультра- и др.)',
                ],
                [
                    "title" => "Название видеоурока с id".($lesson*6-4),
                    "webinarLink" => "<iframe src='https://www.youtube.com/embed/ClUASZ6JHsE'></iframe>",
                    "date" => '2022-07-10 08:00:00',
                    "text" => 'Слова с приставками (включая вне-, nосле-, сверх-, а-, анти-, архи-, инфра-, контр-, ультра- и др.)',
                ],
                [
                    "title" => "Название видеоурока с id".($lesson*6-3),
                    "webinarLink" => "<iframe src='https://www.youtube.com/embed/ClUASZ6JHsE'></iframe>",
                    "date" => '2022-07-10 08:00:00',
                    "text" => 'Слова с приставками',
                ],
                [
                    "title" => "Название видеоурока с id".($lesson*6-2),
                    "webinarLink" => "<iframe src='https://www.youtube.com/embed/ClUASZ6JHsE'></iframe>",
                    "date" => '2022-07-10 08:00:00',
                    "text" => 'Слова с приставками (включая вне-, nосле-, сверх-, а-, анти-, архи-, инфра-, контр-, ультра- и др.)',
                ],
                [
                    "title" => "Название видеоурока с id".($lesson*6-1),
                    "webinarLink" => "<iframe src='https://www.youtube.com/embed/ClUASZ6JHsE'></iframe>",
                    "date" => '2022-07-10 08:00:00',
                    "text" => 'Слова с приставками (включая вне-, nосле-, сверх-, а-, анти-, архи-, инфра-, контр-, ультра- и др.)',
                ],
                [
                    "title" => "Название видеоурока с id".($lesson*6),
                    "webinarLink" => "<iframe src='https://www.youtube.com/embed/ClUASZ6JHsE'></iframe>",
                    "date" => '2022-07-10 08:00:00',
                    "text" => 'Слова с приставками (включая вне-, nосле-, сверх-, а-, анти-, архи-, инфра-, контр-, ультра- и др.)',
                ]
            ];

            foreach ($lesson_webinars as $video_item) {
                LessonWebinar::create($video_item);
            }
        }

    }
}
