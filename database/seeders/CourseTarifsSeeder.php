<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CourseTarif;

class CourseTarifsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        for($i = 1; $i < 5; $i++)
        {
            $couses_tarif[] = [
                "title" => "Тариф Минимум",
                "description" => "Без проверок",
                "type" => 1,
                "subtitle" => 1,
                "courses_id" => $i,
            ];

            $couses_tarif[] = [
                "title" => "Тариф Оптимальный",
                "description" => "Без проверок",
                "type" => 1,
                "subtitle" => 1,
                "courses_id" => $i,
            ];

            $couses_tarif[] = [
                "title" => "Тариф Максимум",
                "description" => "Без проверок",
                "type" => 1,
                "subtitle" => 1,
                "courses_id" => $i,
            ];
        }

        foreach ($couses_tarif as $value) {
            CourseTarif::create($value);
        }
    }
}
