<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(UserSeeder::class);

        $this->call(SubjectSeeder::class);
        $this->call(CourseSeeder::class);
        $this->call(ModuleSeeder::class);
        $this->call(CourseKnowledgeBaseSeeder::class);

        $this->call(CourseTarifsSeeder::class);
        $this->call(CourseTarifsOptionsSeeder::class);
        $this->call(CourseTarifsFeaturesSeeder::class);

        $this->call(OrdersStatusSeeder::class);

        $this->call(CourseUsersTableSeeder::class);

        $this->call(LessonWebinarSeeder::class);
        $this->call(LessonVideoSeeder::class);
        $this->call(LessonSeeder::class);
        $this->call(LessonTypeSeeder::class);
        $this->call(ModuleLessonSeeder::class);
        $this->call(CourseModuleSeeder::class);
        $this->call(UserModuleDonesSeeder::class);
        $this->call(UserSublessonVideosSeeder::class);
        $this->call(UserSublessonWebinarsSeeder::class);
        $this->call(UserLessonDonesSeeder::class);

        $this->call(PivotLessonVideoSeeder::class);
        $this->call(PivotLessonWebinarSeeder::class);
    }
}
