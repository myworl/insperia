<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PivotLessonVideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($lesson_id = 1; $lesson_id <= 24; $lesson_id++) {
            for ($video_id = 1; $video_id <= 16; $video_id++) {

                $data[] = [
                    'lesson_id' => $lesson_id,
                    'video_id' => $video_id
                ];
            }
        }

        DB::table('pivot_lesson_video')->insert($data);
    }
}
