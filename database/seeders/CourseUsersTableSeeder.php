<?php

namespace Database\Seeders;

use App\Models\CourseUser;
use Illuminate\Database\Seeder;

class CourseUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CourseUser::truncate();

        for($user = 1; $user < 3; $user++)
        {
            $my_courses[] = [
                "users_id" => $user,
                "options_id" => 1,
            ];

            $my_courses[] = [
                "users_id" => $user,
                "options_id" => 7,
            ];
        }

        foreach ($my_courses as $value) {
            CourseUser::create($value);
        }
    }
}
