<?php

namespace Database\Seeders;

use App\Models\Module_lesson;
use App\Models\ModuleLesson;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ModuleLessonSeeder extends Seeder
{
    public function run()
    {
        ModuleLesson::truncate();

        for($lesson = 1; $lesson < 27; $lesson++)
        {
            $module_lesson[] = [
                "modules_id" => ceil($lesson / 2),
                "lessons_id" => $lesson,
            ];
        }

        foreach ($module_lesson as $module_lesson_item) {
            ModuleLesson::create($module_lesson_item);
        }
    }
}
