<?php

namespace Database\Seeders;

use App\Models\Course;
use App\Models\User;
use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Course::truncate();

        $courses = [
            [
                "title" => "Годовой курс по информатике 2021-2022",
                "description" => "Полный курс подготовки к ЕГЭ по физике с Яной Ивановой",
                "order" => 1,
                "count_places" => 25,
                "subject_id" => 1,
                "popularity" => 32,
                "thumb" => NULL,
                "image" => NULL,
                "date_start" => "2022-03-10 08:00:00",
                "date_end" => "2022-04-10 20:00:00",
            ],
            [
                "title" => "Годовой курс по математике 2021-2022",
                "description" => "Полный курс подготовки к ЕГЭ по математике с Екатериной Фроловой",
                "order" => 2,
                "count_places" => 25,
                "subject_id" => 2,
                "popularity" => 444,
                "thumb" => NULL,
                "image" => NULL,
                "date_start" => "2022-03-10 08:00:00",
                "date_end" => "2022-04-10 20:00:00",
            ],
            [
                "title" => "Годовой курс по русскому языку 2021-2022",
                "description" => "Полный курс подготовки к ЕГЭ по русскому языку с Анной Солдаевой",
                "order" => 3,
                "count_places" => 25,
                "subject_id" => 3,
                "popularity" => 22,
                "thumb" => NULL,
                "image" => NULL,
                "date_start" => "2022-03-10 08:00:00",
                "date_end" => "2022-04-10 20:00:00",
            ],
            [
                "title" => "Годовой курс по физике 2021-2022 (4)",
                "description" => "Полный курс подготовки к ЕГЭ по физике с Яной Ивановой",
                "order" => 4,
                "count_places" => 25,
                "subject_id" => 4,
                "popularity" => 605,
                "thumb" => NULL,
                "image" => NULL,
                "date_start" => "2022-03-10 08:00:00",
                "date_end" => "2022-04-10 20:00:00",
            ]
        ];



        foreach ($courses as $course) {
             Course::create($course);
        }
    }
}
