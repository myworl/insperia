<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $developer = Role::where('slug','student')->first();
        $manager = Role::where('slug', 'parent')->first();
        $createTasks = Permission::where('slug','create-tasks')->first();
        $manageUsers = Permission::where('slug','manage-users')->first();
        
        $user1 = new User();
        $user1->name = 'Jhon';
        $user1->lastName = "Deo";
        $user1->email = 'user@mail.ru';
        $user1->password = bcrypt('12345678');
        $user1->save();
        $user1->roles()->attach($developer);
        $user1->permissions()->attach($createTasks);
        
        $user2 = new User();
        $user2->name = 'Devid';
        $user2->lastName = "Thomas";
        $user2->email = 'mike@thomas.com';
        $user2->password = bcrypt('12345678');
        $user2->save();
        $user2->roles()->attach($manager);
        $user2->permissions()->attach($manageUsers);
    }
}
