<?php

namespace Database\Seeders;

use App\Models\CourseTarifFeature;
use Illuminate\Database\Seeder;

class CourseTarifsFeaturesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CourseTarifFeature::truncate();

        for($tarif = 1; $tarif < 13; $tarif++)
        {
            $features[] = [
                "title" => "2 беседы",
                "description" => "Без проверок",
                "course_tarif_id" => $tarif,
            ];

            $features[] = [
                "title" => "2 сочинения",
                "description" => "Без проверок",
                "course_tarif_id" => $tarif,
            ];
        }
        

        foreach ($features as $couse_t) {
            CourseTarifFeature::create($couse_t);
        }
    }
}
