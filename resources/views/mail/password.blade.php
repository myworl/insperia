@component('mail::message')
    # Подтверждение почты

    Нажмите на кнопку, что бы подтвердить почту!

        @component('mail::button', ['url' => $url])
            Подтвердить
        @endcomponent


{{--    Thanks,<br>--}}
{{--    {{ config('app.name') }}--}}
@endcomponent
